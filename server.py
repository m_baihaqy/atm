# -*- coding: utf-8 -*-
"""
Created on Thu May  2 14:08:09 2019

@author: asus
"""

# import SimpleXMLRPCServer
from xmlrpc.server import SimpleXMLRPCServer
# import SimpleXMLRPCRequestHandler
from xmlrpc.server import SimpleXMLRPCRequestHandler
import threading
import math
# Batasi hanya pada path /RPC2 saja supaya tidak bisa mengakses path lainnya
data = []
data.append([1,123,100000])
data.append([2,1234,100000])
data.append([3,1235,100000])
class RequestHandler(SimpleXMLRPCRequestHandler):
    rpc_paths = ('/RPC2',)
# Buat server
with SimpleXMLRPCServer(("192.168.43.42", 54321),
                        requestHandler=RequestHandler, allow_none=True) as server:
    server.register_introspection_functions()
    # siapkan lock
    lock = threading.Lock()

    def menu():
        lock.acquire()
        lock.release()
        msg="==============================\n==  MESIN ATM  ==\n==============================\nMenu : \n1. Transfer\n2. Deposit Tunai\n3. Cek Saldo\n4. Penarikan"
        return msg
        lock.release()
    server.register_function(menu)

    def ceksaldo(id, pin):
        lock.acquire()
        lock.release()
        saldo = -1
        for x in data:
            if (x[0]==id) and (x[1]==pin):
                saldo=x[2]
        if (saldo==-1):
            return "Maaf pin salah"
        else:
            return "SALDO ANDA : "+str(saldo)
        lock.release()
    server.register_function(ceksaldo)

    def login(id, pin):
        lock.acquire()
        lock.release()
        user = -1
        for x in data:
            if (x[0]==id) and (x[1]==pin):
                user=1
        if (user==-1):
            return False
        else:
            return True
        lock.release()
    server.register_function(login)

    def tariktunai(id, pin,jumlah):
        lock.acquire()
        lock.release()
        idx = -1
        for i in range(len(data)):
            if (data[i][0]==id) and (data[i][1]==pin):
                idx=i
        if (idx==-1):
            return "Maaf password salah"
        else:
            if (jumlah > data[idx][2]):
                return "maaf saldo tidak cukup"
            else:
                data[idx][2]=data[idx][2]-jumlah
                return "berhasil \nsisa saldo "+str(data[idx][2])
        lock.release()
    server.register_function(tariktunai)

    def depo(id, pin,jumlah):
        lock.acquire()
        lock.release()
        idx = -1
        for i in range(len(data)):
            if (data[i][0]==id) and (data[i][1]==pin):
                idx=i
        if (idx==-1):
            return "Maaf password salah"
        else:
            data[idx][2]=data[idx][2]+jumlah
            return "berhasil \ntotal saldo "+str(data[idx][2])
        lock.release()
    server.register_function(depo)

    def transfer(id,pin,jumlah,idtujuan):
        lock.acquire()
        lock.release()
        idxt= -1
        idx = -1
        for i in range(len(data)):
            if (data[i][0]==id) and (data[i][1]==pin):
                idx=i
            if (data[i][0]==idtujuan):
                idxt=i
        if (idx==-1):
            return "Maaf password salah"
        else:
            if (jumlah > data[idx][2]):
                return "maaf saldo tidak cukup"
            else:
                if (idxt==-1):
                    return "maaf nomer rekening tujuan salah"
                else:
                    data[idx][2]=data[idx][2]-jumlah
                    data[idxt][2]=data[idxt][2]+jumlah
                    return "transfer berhasil \nsisa saldo "+str(data[idx][2])
        lock.release()
    server.register_function(transfer)

    print("Server CAOL Running...")
    # Jalankan server
    server.serve_forever()
